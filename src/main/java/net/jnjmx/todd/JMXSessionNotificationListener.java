package net.jnjmx.todd;

import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.Notification;
import javax.management.NotificationListener;
import javax.management.ObjectInstance;
import javax.management.ObjectName;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXServiceURL;

public class JMXSessionNotificationListener implements NotificationListener {
	@Override
	public void handleNotification(Notification notification, Object handback) {
		System.out.println("Received Notification");
		System.out.println("======================================");
		System.out.println("Timestamp: " + notification.getTimeStamp());
        try
        {
			String server = "192.168.122.5:10500";
			int growSize = 10;

			System.out.println("Connecting to JMX Agent (with running TODD server) at " + server + " ...");

			// Connect to a remote MBean Server
			JMXConnector c = javax.management.remote.JMXConnectorFactory
					.connect(new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + server + "/jmxrmi"));

			MBeanServerConnection mbs = c.getMBeanServerConnection();

			ObjectName objName = new ObjectName("todd:id=SessionPool");

			// Now invoking operation to grow
			// Go through operation to find the interested one and invoke
			mbs.invoke(objName, "grow", new Integer[] { new Integer(growSize) }, new String[] { "int" });
			System.out.println("Grow of " + growSize + " sent to server.");
			c.close();

        }catch(Exception e)
        {
			System.out.println("-----Exception");
			e.printStackTrace();
			System.out.println(e.getMessage());
			System.exit(2);
        }
	}
}

