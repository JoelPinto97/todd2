package net.jnjmx.todd;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.util.Scanner;
import java.util.Set;

import javax.management.MBeanServer;
import javax.management.MBeanServerConnection;
import javax.management.ObjectInstance;
import javax.management.ObjectName;

import javax.management.remote.*;


public class ClientApp6 {

	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {

		System.out.println("Todd ClientApp2... Accessing JMX Beans (and the 'Uptime' property of TODD MBean 'Server')");

		try {
			String server = "192.168.122.5:10500";

			if (args.length >= 1) {
				server = args[0];
			}

			System.out.println("Connecting to JMX Agent at "+server+" ...");

			// Connect to a remote MBean Server
			JMXConnector c = javax.management.remote.JMXConnectorFactory
					.connect(new JMXServiceURL("service:jmx:rmi:///jndi/rmi://" + server + "/jmxrmi"));
			
			MBeanServerConnection mbs = c.getMBeanServerConnection();
			ObjectName spmon = new ObjectName("todd:id=SessionPool");			
			
        	int poolSize = (int) mbs.getAttribute(spmon, "Size");
        	System.out.println("Current Size " + poolSize);
        	int availSessions = (int) mbs.getAttribute(spmon, "AvailableSessions");
        	System.out.println("Current AvailableSessions " + availSessions);

			c.close();
			if(availSessions< (int)(poolSize*0.2))
			{
				System.exit(2);
			}
			System.exit(0);

		} catch (Exception ex) {
			System.out.println("Error: unable to connect to MBean Server");
			System.exit(2);
		}
	}
}
